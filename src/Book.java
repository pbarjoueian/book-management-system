
import java.awt.AWTException;
import java.awt.Robot;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;


public class Book {
    private final static String filePath = "book.txt";
    private String name;
    private int price, year;

    public String getName() {return name;}
    public int getPrice() {return price;}
    public int getYear() {return year;}

    public Book(String name, int price, int year) {
        this.name = name;this.price = price;
        this.year = year;
    }
    
    @Override
    public String toString(){
        return this.name + "\t" + this.price + "\t" + this.year;
    }
    
    public void writeToFile() throws IOException,AlreadyExistException{
        File file = new File(filePath);
        if (!file.exists()){
            file.createNewFile();
        }
        FileReader fileReader = new FileReader(file);
        
        BufferedReader bufferedReader = new BufferedReader(fileReader);
	
        String line,temp_name;
        int temp_price, temp_year;
        
	while ((line = bufferedReader.readLine()) != null) {
            String[] parts = line.split("\t");
            temp_name = parts[0];
            temp_price = Integer.parseInt(parts[1]);
            temp_year = Integer.parseInt(parts[2]);
            try{
                if (this.name.equalsIgnoreCase(temp_name) && this.year == temp_year)
                {
                    throw new AlreadyExistException();
                }
            }catch(AlreadyExistException oee){
                System.out.println(oee.getMessage());
                fileReader.close();
                return;
            } 
        }
        fileReader.close();
        
        FileWriter write = new FileWriter(filePath, true);
        PrintWriter print_line = new PrintWriter(write);
        print_line.printf(this.toString()+"%n");
        print_line.close();
        return;
    }
   
    public static void printAll() throws IOException{
        System.out.println("Name:" + "\t" + "Price:" + "\t" + "Year:");
        File file = new File(filePath);
        if (!file.exists()){
            System.out.println("The file is not exist.");
            return;
        }
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
	
        String line,temp_name;
        int temp_price, temp_year;
        while ((line = bufferedReader.readLine()) != null) {
            
            String[] parts = line.split("\t");
            temp_name = parts[0];
            temp_price = Integer.parseInt(parts[1]);
            temp_year = Integer.parseInt(parts[2]);
            
            System.out.println(new Book(temp_name, temp_price, temp_year).toString());
        }
        fileReader.close();
    }
    
    public static Book getCheapestBook() throws IOException
    {
        File file = new File(filePath);
        if (!file.exists()){
            System.out.println("The file is not exist.");
            return null;
        }
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
	
        String line,temp_name;
        int temp_price, temp_year;
        ArrayList<Book> books = new ArrayList<>();
        while ((line = bufferedReader.readLine()) != null) {
            String[] parts = line.split("\t");
            temp_name = parts[0];
            temp_price = Integer.parseInt(parts[1]);
            temp_year = Integer.parseInt(parts[2]);

            books.add(new Book(temp_name, temp_price, temp_year));
        }
        fileReader.close();
        Book cheapest = books.get(0);
        for (int i=1; i< books.size(); i++)
        {
            if (books.get(i).price< cheapest.price)
            {
                cheapest = books.get(i);
            }
        }
        return cheapest;
    }
    
    public static ArrayList<Book> getBooks(int year) throws FileNotFoundException, IOException{
        File file = new File(filePath);
        if (!file.exists()){
            System.out.println("The file is not exist.");
            return null;
        }
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
	
        String line,temp_name;
        int temp_price, temp_year;
        ArrayList<Book> books = new ArrayList<>();
        
        while ((line = bufferedReader.readLine()) != null) {
            String[] parts = line.split("\t");
            temp_name = parts[0];
            temp_price = Integer.parseInt(parts[1]);
            temp_year = Integer.parseInt(parts[2]);
            
            if (year == temp_year)
            {
                books.add(new Book(temp_name, temp_price, temp_year));
            }
        }
        return books;
    }
    
    
    public static void main(String[] args) throws AlreadyExistException, IOException, AWTException {
        
        InputStreamReader sr = new InputStreamReader(System.in);
        BufferedReader input = new BufferedReader(sr);
        
        while(true)
        {    
            System.out.println("1- Enter information of a new book");
            System.out.println("2- Print information of all books");
            System.out.println("3- Print information of the cheapest book");
            System.out.println("4- Print information of the books in certain year");
            System.out.println("5- Exit");
            System.out.println("Enter your choice:");
            
            String getKey = input.readLine();
            
            switch (getKey){
                case "1":
                    System.out.println("Enter the book name:");
                    String bookName = input.readLine();
                    System.out.println("Enter the book price:");
                    int bookPrice = Integer.parseInt(input.readLine());
                    System.out.println("Enter the book year:");
                    int bookYear = Integer.parseInt(input.readLine());
                    
                    Book newBook = new Book(bookName, bookPrice, bookYear);
                    newBook.writeToFile();
                    System.out.println("Press any key...");
                    break;
                case "2":
                    Book.printAll();
                    System.out.println("Press any key...");
                    input.readLine();
                    break;
                case "3":
                    System.out.println("The cheapest book is:");
                    System.out.println(Book.getCheapestBook().toString());
                    System.out.println("Press any key...");
                    input.readLine();
                    break;
                case "4":
                    System.out.println("Enter the year:");
                    int inputYear = Integer.parseInt(input.readLine());
                    ArrayList<Book> result = Book.getBooks(inputYear);
                    for (Book book: result)
                    {
                        System.out.println(book.toString());
                    }
                    System.out.println("Press any key...");
                    input.readLine();
                    break;  
                case "5": 
                    return;
                default:
                    System.out.println("Please enter a valid key.");
                    break;
            }  
        }
    }
}

